﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class PlayerController : StandaloneInputModule
{
    private Vector2 currentAxis;

    public override void UpdateModule()
    {
        currentAxis = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    public override void Process()
    {
        if (eventSystem.currentSelectedGameObject != null && currentAxis.magnitude > 0.01f)
        {
            ExecuteEvents.Execute(eventSystem.currentSelectedGameObject,
                GetAxisEventData(currentAxis.x, 0.0f, 0.0f), ExecuteEvents.moveHandler);
        }
    }
}

﻿using System;
using UnityEngine;
using System.Collections;

/// <summary>
/// The bounding colliders that form around the game area to control ball movement and report ball loss.
/// </summary>
public class BoundingCollider : MonoBehaviour
{
    public class BallOutEventArgs : EventArgs
    {
        public Ball OutBall { get; private set; }

        public BallOutEventArgs(Ball outBall)
        {
            OutBall = outBall;
        }
    }

    public event EventHandler<BallOutEventArgs> BallOut;

    protected virtual void OnBallOut(Ball ball)
    {
        if (BallOut != null) BallOut(this, new BallOutEventArgs(ball));
    }

    void Start()
    {
        // Set up references:
        var bf = GetComponent<BlockFormation>();

        // Create new colliders:
        var leftColl = gameObject.AddComponent<BoxCollider2D>();
        var rightColl = gameObject.AddComponent<BoxCollider2D>();
        var topColl = gameObject.AddComponent<BoxCollider2D>();
        var bottomColl = gameObject.AddComponent<BoxCollider2D>();

        // Configure the colliders:
        leftColl.size = new Vector3(1.0f, bf.ScreenWorldHeight);
        leftColl.center = new Vector3(-bf.ScreenWorldWidth / 2 - 0.5f, 0.0f);
        leftColl.tag = "Wall";

        rightColl.size = new Vector3(1.0f, bf.ScreenWorldHeight);
        rightColl.center = new Vector3(bf.ScreenWorldWidth / 2 + 0.5f, 0.0f);
        rightColl.tag = "Wall";

        topColl.size = new Vector3(bf.ScreenWorldWidth, 1.0f);
        topColl.center = new Vector3(0.0f, bf.ScreenWorldHeight / 2 + 0.5f);
        topColl.tag = "Wall";

        bottomColl.size = new Vector3(bf.ScreenWorldWidth, 1.0f);
        bottomColl.center = new Vector3(0.0f, -bf.ScreenWorldHeight / 2 - 0.5f);
        bottomColl.isTrigger = true;
        bottomColl.tag = "Wall";
    }

    // Raise the event of a ball exiting the trigger collider (the bottom collider).
    void OnTriggerExit2D(Collider2D other)
    {
        var ball = other.GetComponent<Ball>();
        if (ball != null)
        {
            OnBallOut(ball);
        }
    }
}

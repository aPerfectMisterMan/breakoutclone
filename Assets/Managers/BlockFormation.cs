﻿using System;
using System.IO;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Loads block formation info from a CSV (.txt) file. 
/// and constructs the formation of blocks. 
/// </summary>
public class BlockFormation : MonoBehaviour
{

    #region Variables we want editable in the inspector
    public string blockFormation;
#pragma warning disable 649
    [SerializeField] private GameObject blockPrefab;
    [SerializeField] private List<Color> blockColors;
    [SerializeField] private Bounds offset;
#pragma warning restore 649
    #endregion
    #region Variables we want to hide from the (non-debug) inspector.
    private char[][] _formation;
    
    /// <summary>
    /// Special block types not represented by a number in the CSV file gets their HP values
    ///  from this dictionary. 
    /// </summary>
    private Dictionary<char, int> specialBlockHp = new Dictionary<char, int>
    {
        {'x', 1}
    };

    public float formationWidth { get; private set; }

    public float formationHeight { get; private set; }

    public float ScreenWorldWidth { get; private set; }

    public float ScreenWorldHeight { get; private set; }

    public Color GetBlockColor(int hitpoints)
    {
        return blockColors[hitpoints-1];
    }
    #endregion

    void Awake()
    {
        var sceneTransition = FindObjectOfType<SceneTransition>();
        if (sceneTransition != null)
        {
            blockFormation = sceneTransition.GetInfo("BlockFormation");
        }
        LoadBlockFormation(blockFormation);
    }

    /// <summary>
    /// Loads the bock formation from a CSV file (.txt).
    /// </summary>
    /// <param name="name">The file name.</param>
    public void LoadBlockFormation(string name)
    {
        var blockRowStrings = new List<string>();

        using (var sr = new StreamReader(Path.Combine(Application.streamingAssetsPath, name)))
        {
            blockRowStrings.AddRange(sr.ReadToEnd().Split(new[] { "\r\n", "\n" }, StringSplitOptions.None));
        }

        // Allocate the rows...
        var loadedBlockFormation = new char[blockRowStrings.Count][];

        // Allocate the columns...
        for (var y = 0; y < blockRowStrings.Count; y++)
        {
            loadedBlockFormation[y] = Array.ConvertAll<string, char>(blockRowStrings[y].Split(',').ToArray(), char.Parse);
        }

        _formation = loadedBlockFormation;
    }

    /// <summary>
    /// Construct the blocks in the correct location and formation.
    /// </summary>
    void Start()
    {
        var blockSpriteRenderer = blockPrefab.GetComponent<SpriteRenderer>();

        // The size of the screen in world space:
        ScreenWorldHeight = Camera.main.orthographicSize * 2.0f;
        ScreenWorldWidth = ScreenWorldHeight / Screen.height * Screen.width;

        // The size of the formation bounds in world space:
        formationHeight = ScreenWorldHeight * offset.extents.y;
        formationWidth = ScreenWorldWidth * offset.extents.x;

        // The block prefab bounds:
        var prefabWidth = blockSpriteRenderer.bounds.size.x;
        var prefabHeight = blockSpriteRenderer.bounds.size.y;

        // Creating the blocks in formation:
        var rowNum = _formation.Length;
        for (var row = 0; row < rowNum; row++)
        {
            var colNum = _formation[row].Length;
            for (var col = 0; col < colNum; col++)
            {
                var blockType = _formation[row][col];

                // White-space we don't use.
                if (Char.IsWhiteSpace(blockType))
                {
                    continue;
                }

                // Block scale:
                var blockScale = new Vector2(
                    formationWidth / prefabWidth / colNum,
                    formationHeight / prefabHeight / rowNum);

                // Block position:
                var blockPos = new Vector2(
                    -ScreenWorldWidth / 2.0f + (prefabWidth / 2 * blockScale.x) + (prefabWidth * blockScale.x) * col + offset.center.x * formationWidth,
                    ScreenWorldHeight / 2.0f - (prefabHeight / 2 * blockScale.y) - (prefabHeight * blockScale.y) * row + offset.center.y * formationHeight);

                // Instiantiate and configure block:
                var newBlockGo = (GameObject)Instantiate(blockPrefab, blockPos, Quaternion.identity);
                var newBlockCm = newBlockGo.GetComponent<Block>();

                newBlockGo.transform.localScale = blockScale;

                if (Char.IsNumber(blockType))
                {
                    newBlockCm.ChangeHitPoints((int) Char.GetNumericValue(_formation[row][col]));
                    newBlockCm.Type = Block.BlockType.Normal;
                }
                else if (blockType == 'x')
                {
                    newBlockCm.ChangeHitPoints(specialBlockHp['x']);
                    newBlockCm.Type = Block.BlockType.PowerUp;
                }
                else
                {
                    throw new FormatException("blockType: '" + blockType + "' could not be parsed.");
                }
            }
        }
    }
}

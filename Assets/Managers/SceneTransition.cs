﻿using UnityEngine;
using System.Collections.Generic;

public class SceneTransition : MonoBehaviour {

    private Dictionary<string, string> _info = new Dictionary<string, string>(); 

	void Start () {
	    DontDestroyOnLoad(gameObject);
	}

    public string GetInfo(string Key)
    {
        return _info[Key];
    }

    public void SetInfo(string key, string value)
    {
        _info[key] = value;
    }
}

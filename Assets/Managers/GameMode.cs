﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Object = UnityEngine.Object;

/// <summary>
/// GameMode handles the overarching events of the game such as lives lost,
/// checking win and lose conditions, starting new rounds, etc.
/// </summary>
public class GameMode : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _announceText;
    [SerializeField] private List<GameObject> heartIcons = new List<GameObject>();
    [SerializeField] private GameObject _ballPrefab;
    [SerializeField] private GameState _state = GameState.Starting;
    [SerializeField] private GameObject _powerUpPrefab;
#pragma warning restore 649

    private int _blocksRemaining;
    private Player _player;
    private PlayerController _playerController;
    private readonly List<Ball> _balls = new List<Ball>();
    private BoundingCollider _boundingCollider;

    public enum GameState
    {
        Starting,
        Playing,
        Paused,
        GameOver,
        Win
    }

    void Awake()
    {
        // Setup references:
        _player = FindObjectOfType<Player>();
        _playerController = FindObjectOfType<PlayerController>();
        _balls.AddRange(FindObjectsOfType<Ball>());
        _boundingCollider = FindObjectOfType<BoundingCollider>();

        // Setup event handlers:
        _player.Lifeless += OnLifeless;
        _player.LifeLoss += OnLifeLoss;
        _balls.ForEach(b => b.BlockDestroyed += OnBlockDestroyed);
        _boundingCollider.BallOut += OnBallOut;
    }

    /// <summary>
    /// Handle the event that is raised when a ball exits the lower part of the screen area.
    /// </summary>
    /// <param name="sender">The collider mananger.</param>
    /// <param name="e">EventArgs about the exiting ball.</param>
    void OnBallOut(object sender, BoundingCollider.BallOutEventArgs e)
    {
        _balls.Remove(e.OutBall);
        e.OutBall.BlockDestroyed -= OnBlockDestroyed;
        Destroy(e.OutBall.gameObject);
        if (_balls.Count == 0)
        {
            _player.Kill();
        }
    }

    void Start()
    {
        _announceText.gameObject.SetActive(false);
        _blocksRemaining = FindObjectsOfType<Block>().Length;
    }

    /// <summary>
    /// Handle the event of a block being destroyed, points are given, win conditions being checked, etc.
    /// </summary>
    /// <param name="sender">The event raiser object.</param>
    /// <param name="e">EventArgs about the destroyed block.</param>
    void OnBlockDestroyed(object sender, Ball.BlockDestroyedEventArgs e)
    {
        _player.Points++;
        _scoreText.text = "Score: " + _player.Points;
        _blocksRemaining--;
        if (e.DestroyedType == Block.BlockType.PowerUp)
        {
            var pU = (GameObject) Instantiate(_powerUpPrefab, e.Position, Quaternion.identity);
            pU.GetComponent<PowerUp>().Collected += OnPowerUpCollected;
        }
        if (_blocksRemaining < 1)
        {
            Win();
        }
    }

    /// <summary>
    /// Handle the event of a PowerUp being collected by the player.
    /// </summary>
    /// <param name="sender">The event raiser object.</param>
    /// <param name="e">EventArgs.</param>
    private void OnPowerUpCollected(object sender, EventArgs e)
    {
        const float offset = 0.1f;
        for (var i = 0; i < 3; i++)
        {
            var newBall = (GameObject) Instantiate(_balls[0].gameObject);
            var newBallCm = newBall.GetComponent<Ball>();
            newBallCm.Owner = _player;
            newBall.transform.Translate(offset * i, 0.0f,0.0f);
            _balls.Add(newBallCm);
            newBallCm.BlockDestroyed += OnBlockDestroyed;
            newBall.transform.Translate(new Vector3((i - 1.5f) * offset, offset));
        }
    }

    /// <summary>
    /// Set the state of the game to a win state, show announcement, pauses the game.
    /// </summary>
    private void Win()
    {
        _state = GameState.Win;
        ShowAnnouncement("You Win!");
        PauseGame();
        StartCoroutine(ReturnToMenu());
    }

    /// <summary>
    /// Set the state of the game to a GameOver state, show announcement, pause the game.
    /// </summary>
    private void Lose()
    {
        _state = GameState.GameOver;
        ShowAnnouncement("Game Over!");
        heartIcons.ForEach(e => e.SetActive(false));
        PauseGame();
        StartCoroutine(ReturnToMenu());

    }

    private IEnumerator<WaitForSeconds> ReturnToMenu()
    {
        yield return new WaitForSeconds(5.0f);
        Application.LoadLevel("Menu");
    }

    /// <summary>
    /// Deactivates player control and ball movement.
    /// </summary>
    private void PauseGame()
    {
        _playerController.DeactivateModule();
        _balls.ForEach(b => b.gameObject.SetActive(false));
        //TODO: Should probably also deactivate powerups.
    }

    /// <summary>
    /// Activates player control again along with balls in the scene.
    /// </summary>
    private void UnpauseGame()
    {
        _playerController.ActivateModule();
        _balls.ForEach(b => b.gameObject.SetActive(true));
    }

    /// <summary>
    /// Shows text on the screen using UnityGUI.
    /// </summary>
    /// <param name="text"></param>
    private void ShowAnnouncement(string text)
    {
        _announceText.text = text;
        _announceText.gameObject.SetActive(true);
    }

    /// <summary>
    /// Hides the text being shown on the screen.
    /// </summary>
    private void HideAnnouncement()
    {
        _announceText.text = string.Empty;
        _announceText.gameObject.SetActive(false);
    }

    /// <summary>
    /// Handles a life being lost (but not nessesarily ending the game).
    /// </summary>
    /// <param name="sender">The event raiser object.</param>
    /// <param name="eventArgs">EventArgs (unhandled).</param>
    private void OnLifeLoss(object sender, EventArgs eventArgs)
    {
        _state = GameState.Paused;
        ShowAnnouncement("Life lost!");
        PauseGame();
        StartCoroutine(NewLife());
        heartIcons[_player.Lives].SetActive(false);
        FindObjectsOfType<PowerUp>().ToList().ForEach(pu => Destroy(pu.gameObject));
    }

    /// <summary>
    /// Handles the event of lives being 0.
    /// </summary>
    /// <param name="sender">The event raiser object.</param>
    /// <param name="eventArgs">EventArgs (unhandled).</param>
    private void OnLifeless(object sender, EventArgs eventArgs)
    {
        Lose();
    }

    /// <summary>
    /// Begins a new life after a life being lost.
    /// </summary>
    /// <returns>IEnumerator used for unity coroutine functionality.</returns>
    private IEnumerator<WaitForSeconds> NewLife()
    {
        yield return new WaitForSeconds(2.0f);
        _player.Reset();
        var ballGo = (GameObject)Instantiate(_ballPrefab, new Vector3(0.0f, 1.0f), Quaternion.identity);
        var ballCm = ballGo.GetComponent<Ball>();
        _balls.Add(ballCm);
        ballCm.Owner = _player;
        ballCm.BlockDestroyed += OnBlockDestroyed;
        ballGo.SetActive(false);
        ShowAnnouncement("New life, Get Ready!");
        yield return new WaitForSeconds(2.0f);
        _state = GameState.Playing;
        UnpauseGame();
        HideAnnouncement();
        ballGo.SetActive(true);
    }
}
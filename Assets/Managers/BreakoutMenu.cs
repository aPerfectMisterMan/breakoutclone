﻿using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BreakoutMenu : MonoBehaviour {
    [SerializeField] private GameObject levelButtonPrefab;
    private GameObject canvasGo;

    void Awake()
    {
        canvasGo = FindObjectOfType<Canvas>().gameObject;
    }

	void Start ()
	{
	    var buttonCount = 0;
	    Directory.GetFiles(Application.streamingAssetsPath, "BlockFormation*.txt")
            .Select(path => Path.GetFileName(path))
	        .ToList()
	        .ForEach(name =>
	        {
	            buttonCount++;
                // Make a new button as child of the LevelPanel and set its name.
	            var newButton = (GameObject) Instantiate(levelButtonPrefab, new Vector3(0, 0), Quaternion.identity);
	            newButton.transform.parent = GameObject.Find("LevelPanel").transform;
	            newButton.GetComponentInChildren<Text>().text = name;

                // Set the anchor in the center top of the LevelPanel
	            var rect = newButton.GetComponent<RectTransform>();
                rect.anchorMin = new Vector2(0.5f, 1.0f);
                rect.anchorMax = new Vector2(0.5f, 1.0f);

                // Get size of the button.
	            var sizeY = newButton.GetComponent<Image>().sprite.bounds.size.y * 200;

                // Set position of the button.
                rect.anchoredPosition = new Vector3(0,-sizeY * buttonCount);

                // Add listener to click handler function.
                newButton.GetComponent<Button>().onClick.AddListener(() =>
                {
                    FindObjectOfType<SceneTransition>().SetInfo("BlockFormation", name);
                    Application.LoadLevel("Game");
                });
	        });
	}
}

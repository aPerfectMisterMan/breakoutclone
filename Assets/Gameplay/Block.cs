﻿using System;
using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// A block is created by a manager and does not contain any exciting logic.
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class Block : MonoBehaviour
{
    // Variables we want to show in the inspector.
    [SerializeField] private int hitPoints;

    // Variables we don't want to show in the (non-debug) inspector.
    private SpriteRenderer spriteRenderer;
    private BlockFormation blockFormation;
    public BlockType Type { get; set; }

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        blockFormation = FindObjectOfType<BlockFormation>();
    }

	// Use this for initialization
	void Start () {
	    UpdateColor();
	}

    /// <summary>
    /// Changes the HitPoint value.
    /// </summary>
    /// <param name="amount">The amount to change the current hp value by.</param>
    /// <returns>The new HitPoint value.</returns>
    public int ChangeHitPoints(int amount)
    {
        hitPoints += amount;
        if (hitPoints == 0)
        {
            Destroy(gameObject);
            return hitPoints;
        }
        UpdateColor();
        return hitPoints;
    }

    /// <summary>
    /// Updates the color of the block according the the hitpoint value.
    /// </summary>
    private void UpdateColor()
    {
        spriteRenderer.color = blockFormation.GetBlockColor(hitPoints);
    }

    /// <summary>
    /// The type of the block (normal ones that give points or PowerUp spawning blocks).
    /// </summary>
    public enum BlockType
    {
        Normal,
        PowerUp
    }
}

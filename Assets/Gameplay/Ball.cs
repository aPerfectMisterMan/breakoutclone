﻿using System;
using UnityEngine;

/// <summary>
/// The ricocheting ball is updated in this script.
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class Ball : MonoBehaviour, IResetable
{
    // Variables we want to see AND change in the inspector.
    [SerializeField] private Vector2 moveDir;
    [SerializeField] private float speedIncrease = 0.005f;
    [SerializeField] private float horizontalSpeedBoost = 0.0f;
    [SerializeField] private float speed = 0.1f;

    // Variables we want to be unchangeable in the editor.
    private GameObject playerPad;
    private Vector2 prevPos;
    private Vector2 _startDir;
    private float _startSpeed;
    private Vector3 _startScale;
    private Quaternion _startRotation;
    private Vector3 _startPosition;

    public float Speed
    {
        get { return speed; }
    }
    public Player Owner { get; set; }
    public event EventHandler<BlockDestroyedEventArgs> BlockDestroyed;

    /// <summary>
    /// EventArgs derivative that has the type and position of a destroyed block.
    /// </summary>
    public class BlockDestroyedEventArgs : EventArgs
    {
        public Block.BlockType DestroyedType { get; private set; }
        public Vector2 Position { get; private set; }

        public BlockDestroyedEventArgs(Block.BlockType destroyedType, Vector2 position)
        {
            Position = position;
            DestroyedType = destroyedType;
        }
    }

    /// <summary>
    /// Raise the event of a block being destroyed.
    /// </summary>
    /// <param name="blockType">The type of block being destroyed (Regular or containing a powerup).</param>
    /// <param name="position">The position of the block that was destroyed.</param>
    protected virtual void OnBlockDestroyed(Block.BlockType blockType, Vector2 position)
    {
        var handler = BlockDestroyed;
        if (handler != null) handler(this, new BlockDestroyedEventArgs(blockType, position));
    }

    void Start()
    {
        // Set values that can be resat to.
        _startDir = moveDir;
        _startSpeed = speed;
        _startScale = transform.localScale;
        _startRotation = transform.localRotation;
        _startPosition = transform.position;
    }

    private void FixedUpdate()
    {
        // Handle movement at a fixed update rate.
        prevPos = transform.position;
        transform.Translate(moveDir * speed * (1 + horizontalSpeedBoost));
    }

    /// <summary>
    /// Handles the Unity event of the ball colliding with something (e.g. Player, Blocks, other balls, etc.)
    /// </summary>
    /// <param name="coll">The Collision2D object created when the collision occurred.</param>
    private void OnCollisionEnter2D(Collision2D coll)
    {
        transform.position = prevPos;
        switch (coll.collider.tag)
        {
            case "Player":
                var padHalfWidth = coll.collider.GetComponent<SpriteRenderer>().bounds.extents.x;
                var fromCenterNormalized = (coll.contacts[0].point.x - coll.collider.transform.position.x)/padHalfWidth;
                moveDir = Vector3.Reflect(moveDir, coll.contacts[0].normal);
                var moveSkew = new Vector2(fromCenterNormalized * 0.9f, 1 - Mathf.Abs(fromCenterNormalized * 0.9f));
                moveSkew.Normalize();
                moveDir = (moveDir + moveSkew).normalized;
                horizontalSpeedBoost = Mathf.Abs(moveSkew.x);
                break;

            case "Block":
                var block = coll.collider.GetComponent<Block>();
                if (block == null) return;
                var remainingBlockHp = block.ChangeHitPoints(-1);
                if (remainingBlockHp == 0)
                {
                    OnBlockDestroyed(block.Type, block.transform.position);
                }
                moveDir = Vector3.Reflect(moveDir, coll.contacts[0].normal);
                break;

            case "Wall":
                moveDir = Vector3.Reflect(moveDir, coll.contacts[0].normal);
                break;
        }
        speed += speedIncrease;
    }

    /// <summary>
    /// Resets values back to what they were at the Start() method call.
    /// </summary>
    public void Reset()
    {
        speed = _startSpeed;
        moveDir = new Vector2(_startDir.x, _startDir.y);
        transform.localScale = _startScale;
        transform.rotation = _startRotation;
        transform.position = _startPosition;
    }

    /// <summary>
    /// Draws the movement direction of the ball in the scene view.
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawRay(transform.position, moveDir * speed * (1 + horizontalSpeedBoost) * 10.0f);
    }
}

﻿using System;
using UnityEngine;

/// <summary>
/// The PowerUp script is simple and just controls the falling motion of the object
/// along with collision with the player GameObject.
/// </summary>
public class PowerUp : MonoBehaviour
{
    private Player player;
    public event EventHandler Collected;
    [SerializeField] private float fallSpeed = 5.0f; 

    /// <summary>
    /// Raises an event when the PowerUp is collected.
    /// </summary>
    protected virtual void OnCollected()
    {
        if (Collected != null) Collected(this, EventArgs.Empty);
    }

    void Awake()
    {
        // Set up references.
        player = FindObjectOfType<Player>();
    }
   
    /// <summary>
    /// Handles the Unity event of a collison happening in 2D space.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject != player.gameObject) return;
        Destroy(gameObject);
        OnCollected();
    }

    void Update()
    {
        // Simple falling movement.
        transform.Translate(new Vector2(0.0f, -fallSpeed * Time.deltaTime));
    }
}

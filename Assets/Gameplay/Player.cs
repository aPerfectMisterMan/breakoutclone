﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// The Player script is the main container of information and logic about the player GameObject.
/// </summary>
public class Player : MonoBehaviour, IMoveHandler, IResetable
{
    // Variables we want to be editable in the inspector.
    [SerializeField] private int lives;
    [SerializeField] private int points = 0;
    [SerializeField] private float speed = 5.0f;
    
    // Variables we don't want to be editable in the inspector.
    private int startLives = 5;
    private float MoveMaxX;
    private float colliderWidth;
    private Vector3 startScale;
    private Quaternion startRotation;
    private Vector3 startPosition;

    public int Points
    {
        get { return points; }
        set { points = value; }
    }

    public int Lives
    {
        get { return lives; }
    }

    void Start()
    {
        // Store start values.
        startLives = lives;
        startScale = transform.localScale;
        startRotation = transform.localRotation;
        startPosition = transform.position;

        MoveMaxX = FindObjectOfType<BlockFormation>().ScreenWorldWidth/2.0f - GetComponent<SpriteRenderer>().bounds.extents.x;
    }

    /// <summary>
    /// Resets fields to the values they had at Start() call.
    /// </summary>
    public void Reset()
    {
        transform.localScale = startScale;
        transform.rotation = startRotation;
        transform.position = startPosition;
    }
    
    /// <summary>
    /// LifeLoss is raised when this Player loses a lives.
    /// </summary>
    public event EventHandler LifeLoss;
    protected virtual void OnLifeLoss()
    {
        if (LifeLoss != null) LifeLoss(this, EventArgs.Empty);
    }

    /// <summary>
    /// Lifeless is raised when this Player has no more lives left.
    /// </summary>
    public event EventHandler Lifeless;
    protected virtual void OnLifeless()
    {
        if (Lifeless != null) Lifeless(this, EventArgs.Empty);
    }

    /// <summary>
    /// Handles Move events from the EventSystem via the Input module.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnMove(AxisEventData eventData)
    {
        var moveVec = eventData.moveVector;
        moveVec *= Time.deltaTime * speed;
        var moveX = moveVec.x;
        var posX = transform.position.x;

        if (posX + moveX > MoveMaxX)
        {
            transform.position = new Vector2(MoveMaxX, transform.position.y);
        } else if (posX + moveX < -MoveMaxX)
        {
            transform.position = new Vector2(-MoveMaxX, transform.position.y);
        }
        else
        {
            transform.Translate(moveVec);            
        }
    }

    /// <summary>
    /// Removes a lives an raises the LifeLoss event delegates.
    /// </summary>
    public void Kill()
    {
        lives -= 1;
        if (lives < 1)
        {
            OnLifeless();
        }
        else
        {
            OnLifeLoss();
        }
    }
}